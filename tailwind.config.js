/*
 ** TailwindCSS Configuration File
 **
 ** Docs: https://tailwindcss.com/docs/configuration
 ** Default: https://github.com/tailwindcss/tailwindcss/blob/master/stubs/defaultConfig.stub.js
 */
module.exports = {
  theme: {
    screens: {
      sm: { min: '600px' },
      md: { min: '960px' },
      lg: { min: '1264px' },
      xl: { min: '1904px' },
    },
    extend: {
      colors: {
        primary: { default: '#181000', light: '#181000', lighter: '#181000' },
        'telegram-blue': '#35ADE1',
        // 'twitter-blue': '#38B8FF',
      },
    },
  },
  variants: {
    flexDirection: ['responsive', 'even'],
    width: ['responsive', 'odd'],
  },
  plugins: [],
  purge: {
    // Learn more on https://tailwindcss.com/docs/controlling-file-size/#removing-unused-css
    enabled: process.env.NODE_ENV === 'production',
    content: [
      'components/**/*.vue',
      'layouts/**/*.vue',
      'pages/**/*.vue',
      'plugins/**/*.js',
      'nuxt.config.js',
    ],

    whitelist: [
      'aos-init',
      'aos-animate',
      'data-aos-delay',
      'data-aos-duration',
      'fade-up',
      'fade-left',
      'fade-right',
      'flip-left',
    ],
  },
}
