// document.addEventListener('DOMContentLoaded', function () {
//   // Check if element is scrolled into view
//   function isScrolledIntoView(elem) {
//     const docViewTop = window.pageYOffset
//     const docViewBottom = docViewTop + window.innerHeight
//     const elemTop = elem.offsetTop
//     const elemBottom = elemTop + elem.clientHeight
//     return elemBottom <= docViewBottom && elemTop >= docViewTop
//   }
//   // If element is scrolled into view, fade it in
//   window.addEventListener('scroll', function () {
//     for (
//       let i = 0;
//       i < document.getElementsByClassName('animate__animated').length;
//       i++
//     ) {
//       const element = document.getElementsByClassName('animate__animated')[i]
//       if (isScrolledIntoView(element) === true) {
//         // console.log(element.getAttribute('myaos'))
//         element.classList.remove('opacity-0')
//         element.classList.add(element.getAttribute('myaos'))
//       }
//     }
//   })
// })

// const animateHTML = function () {
//   let elems, windowHeight

//   const init = function () {
//     elems = document.getElementsByClassName('animate__animated')
//     windowHeight = window.innerHeight
//     _addEventHandlers()
//   }

//   const _addEventHandlers = function () {
//     window.addEventListener('scroll', _checkPosition)
//     window.addEventListener('resize', init)
//   }
//   const _checkPosition = function () {
//     for (let i = 0; i < elems.length; i++) {
//       const posFromTop = elems[i].getBoundingClientRect().top
//       if (posFromTop - windowHeight <= 0) {
//         elems[i].className = elems[i].className.replace(
//           'opacity-0',
//           elems[i].getAttribute('myaos')
//         )
//       }
//     }
//   }

//   return {
//     init,
//   }
// }

// animateHTML().init()
// const aosScript = document.createElement('script')
// aosScript.setAttribute('src', 'https://unpkg.com/aos@2.3.1/dist/aos.js')
// document.head.appendChild(aosScript)
AOS.init({ startEvent: 'load', once: true })
